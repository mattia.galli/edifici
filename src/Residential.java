public class Residential extends Edificio {

    private static final long serialVersionUID = 1L;

    public Residential(String street, String city, String state, String type, String sale_date, int zip, int beds,
            int baths, int sq_ft, int price, double latitude, double longitude) {
        super(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
    }

    public Residential(Residential r) {
        super(r);
    }

    @Override
    public String toString() {
        String s = super.toString();

        return s;
    }

    @Override
    public Residential clone() {
        return new Residential(getStreet(), getCity(), getState(), getType(), getSale_date(), getZip(), getBeds(),
                getBaths(), getSq_ft(), getPrice(), getLatitude(), getLongitude());
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof Residential) {
                Residential r = (Residential) o;
                return super.equals(r);
            }
        }
        return false;
    }
}