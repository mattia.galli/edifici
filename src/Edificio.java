import java.io.Serializable;

public abstract class Edificio  implements Serializable, Comparable<Edificio> {

    private static final long serialVersionUID = 1L;
    
    private String street, city, state, type, sale_date;
    private int zip, beds, baths, sq_ft, price;
    private double latitude, longitude;

    public Edificio(String street, String city, String state, String type, String sale_date, int zip, int beds, int baths, int sq_ft, int price, double latitude, double longitude) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.type = type;
        this.sale_date = sale_date;
        this.zip = zip;
        this.beds = beds;
        this.baths = baths;
        this.sq_ft = sq_ft;
        this.price = price;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Edificio(Edificio e) {
        street = e.getStreet();
        city = e.getCity();
        state = e.getState();
        type = e.getType();
        sale_date = e.getSale_date();
        zip = e.getZip();
        beds = e.getBeds();
        baths = e.getBaths();
        sq_ft = e.getSq_ft();
        price = e.getPrice();
        latitude = e.getLatitude();
        longitude = e.getLongitude();
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSale_date() {
        return this.sale_date;
    }

    public void setSale_date(String sale_date) {
        this.sale_date = sale_date;
    }

    public int getZip() {
        return this.zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public int getBeds() {
        return this.beds;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    public int getBaths() {
        return this.baths;
    }

    public void setBaths(int baths) {
        this.baths = baths;
    }

    public int getSq_ft() {
        return this.sq_ft;
    }

    public void setSq_ft(int sq_ft) {
        this.sq_ft = sq_ft;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public abstract Edificio clone();

    @Override
    public boolean equals(Object o){
        if(o != null){
            if (o instanceof Edificio){
                Edificio e = (Edificio) o;
                return this.street.equals(e.getStreet()) && this.city.equals(e.getCity()) && this.state.equals(e.getState()) && this.type.equals(e.getType()) && this.sale_date.equals(e.getSale_date()) && this.type.equals(e.getType()) && this.zip == e.getZip() && this.beds == e.getBeds() && this.baths == e.getBaths() && this.sq_ft == e.getSq_ft() && this.price == e.getPrice() && this.latitude == e.getLatitude() && this.longitude == e.getLongitude();
            }
        }
        return false;
    }

    @Override
    public int compareTo(Edificio e){
        return Integer.compare(this.getPrice(), e.getPrice());
    }
    
    @Override
    public String toString() {
        return
            "street : " + getStreet() + System.lineSeparator() +
            "city : " + getCity() + System.lineSeparator() +
            "state : " + getState() + System.lineSeparator() +
            "type : " + getType() + System.lineSeparator() +
            "sale_date : " + getSale_date() + System.lineSeparator() +
            "zip : " + getZip() + System.lineSeparator() +
            "beds : " + getBeds() + System.lineSeparator() +
            "baths : " + getBaths() + System.lineSeparator() +
            "sq_ft : " + getSq_ft() + System.lineSeparator() +
            "price : " + getPrice() + System.lineSeparator() +
            "latitude : " + getLatitude() + System.lineSeparator() +
            "longitude : " + getLongitude() + System.lineSeparator();
    }
}