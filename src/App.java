import java.io.*;
import java.util.Scanner;
public class App {

    public static GestionePatrimoniale g = new GestionePatrimoniale();
    public static void main(String[] args) throws Exception {
        g.loadCSV("Dati.csv");

        Scanner in = new Scanner(System.in);
        menu(in, g);
    }
    public static void menu(Scanner in, GestionePatrimoniale g) throws ClassNotFoundException, IOException {
        int scelta;
        
        String street, city, state, type, sale_date;
        int zip, beds, baths, sq_ft, price;
        double latitude, longitude;
        do {
            System.out.println("1) aggiungere edifici");
            System.out.println("2) eliminare l'edificio");
            System.out.println("3) stampare l'elenco degli edifici");
            System.out.println("4) riordinazione degli edifici in base al prezzo (crescente)");
            System.out.println("5) salvare i dati in formato binario");
            System.out.println("6) caricare i dati salvati in formato binario");
            System.out.println("0) chiudi programma");
            scelta = in.nextInt();
            switch (scelta) {

                case 1: 
                    System.out.println("inserire street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude");
                    street = in.next();
                    city = in.next();
                    state = in.next();
                    type = in.next();
                    sale_date = in.next();
                    zip = in.nextInt();
                    beds = in.nextInt();
                    baths = in.nextInt();
                    sq_ft = in.nextInt();
                    price = in.nextInt();
                    latitude = in.nextDouble();
                    longitude = in.nextDouble();
                    switch(type){
                        //(String street, String city, String state, String type, String sale_date, int zip, int beds, int baths, int sq_ft, int price, double latitude, double longitude
                        case "Residential": 
                            Residential r = new Residential(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.aggiungi(r);
                            break;
                        case "Condo": 
                            Condo c = new Condo(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.aggiungi(c);
                            break;
                        case "Multi-Family": 
                            MultiFamily m = new MultiFamily(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.aggiungi(m);
                            break;
                        case "Unkown": 
                            Unknown u = new Unknown(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.aggiungi(u);
                            break;
                        default:                   
                    }
                    break;

                case 2:
                System.out.println("inserire street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude");

                    street = in.next();
                    city = in.next();
                    state = in.next();
                    type = in.next();
                    sale_date = in.next();
                    zip = in.nextInt();
                    beds = in.nextInt();
                    baths = in.nextInt();
                    sq_ft = in.nextInt();
                    price = in.nextInt();
                    latitude = in.nextDouble();
                    longitude = in.nextDouble();
                    switch(type){
                        case "Residential": 
                            Residential r = new Residential(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.rimuovi(r);
                            break;
                        case "Condo": 
                            Condo c = new Condo(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.rimuovi(c);
                            break;
                        case "Multi-Family": 
                            MultiFamily m = new MultiFamily(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.rimuovi(m);
                            break;
                        case "Unkown": 
                            Unknown u = new Unknown(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
                            g.rimuovi(u);
                            break;
                        default:                   
                    }
                    break;
                case 3:
                    System.out.println("Stampa...");
                    System.out.println(g.toString());
                    break;
                case 4:
                    System.out.println("Riordino...");
                    g.sort();
                    break;
                case 5:
                    System.out.println("Salvo...");
                    g.save();
                    break;
                case 6:
                    System.out.println("Carico...");
                    g.load();
                    break;
                default:
            }
        } while (scelta != 0);
    }
}
