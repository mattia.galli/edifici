import java.util.*;
import java.io.*;

public class GestionePatrimoniale {

    private ArrayList<Edificio> lista = new ArrayList<>();
 

    public void loadCSV(final String nomefile) throws IOException {
        Scanner scanner = new Scanner(new File(nomefile)).useDelimiter(System.lineSeparator());
        try {
            while (scanner.hasNext()) {
                final String[] valore = scanner.next().split(";");

                switch(valore[7]){
                  //(String street, String city, String state, String type, String sale_date, int zip, int beds, int baths, int sq_ft, int price, double latitude, double longitude
                  case "Residential": 
                     lista.add(new Residential(valore[0],valore[1],valore[3],valore[7],valore[8],Integer.valueOf(valore[2]),Integer.valueOf(valore[4]),Integer.valueOf(valore[5]),Integer.valueOf(valore[6]),Integer.valueOf(valore[9]),Double.valueOf(valore[10]),Double.valueOf(valore[11])));
                     break;
                  case "Condo": 
                      lista.add(new Condo(valore[0],valore[1],valore[3],valore[7],valore[8],Integer.valueOf(valore[2]),Integer.valueOf(valore[4]),Integer.valueOf(valore[5]),Integer.valueOf(valore[6]),Integer.valueOf(valore[9]),Double.valueOf(valore[10]),Double.valueOf(valore[11])));
                     break;
                  case "Multi-Family": 
                      lista.add(new MultiFamily(valore[0],valore[1],valore[3],valore[7],valore[8],Integer.valueOf(valore[2]),Integer.valueOf(valore[4]),Integer.valueOf(valore[5]),Integer.valueOf(valore[6]),Integer.valueOf(valore[9]),Double.valueOf(valore[10]),Double.valueOf(valore[11])));
                     break;
                  case "Unkown": 
                     lista.add(new Unknown(valore[0],valore[1],valore[3],valore[7],valore[8],Integer.valueOf(valore[2]),Integer.valueOf(valore[4]),Integer.valueOf(valore[5]),Integer.valueOf(valore[6]),Integer.valueOf(valore[9]),Double.valueOf(valore[10]),Double.valueOf(valore[11])));
                    break;
                  default:
                  }
            }
                
        } catch (Exception e) {
          e.printStackTrace();

        } finally{
            scanner.close();
        }
    }
    
    public void aggiungi(final Edificio e) {
        lista.add(e.clone());
    }

    public void rimuovi(final Edificio e) {
        lista.remove(e);
    }

    public int getSize(){
        return lista.size();
    }

    public Edificio getEdificio(int i){
        return this.lista.get(i);
    }

    public void save() throws java.io.IOException {
        final FileOutputStream file = new FileOutputStream(new File("lista.bin"));
        final ObjectOutputStream stream = new ObjectOutputStream(file);
        stream.writeObject(lista);
        file.close();
        stream.close();
    }

    @SuppressWarnings("unchecked")
    public void load() throws IOException, ClassNotFoundException {
        final FileInputStream file = new FileInputStream(new File("lista.bin"));
        final ObjectInputStream stream = new ObjectInputStream(file);

        lista = (ArrayList<Edificio>) stream.readObject();
    }

    public void sort(){
        Collections.sort(lista);
    }

    public String toString(){
        String s = "";
        for (Edificio e: lista){
            s += e.toString() + System.lineSeparator();
        }
        return s;
    }

	public ArrayList<Edificio> getLista() {
		return lista;
	}
}