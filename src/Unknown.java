public class Unknown extends Edificio {

    private static final long serialVersionUID = 1L;

    public Unknown(String street, String city, String state, String type, String sale_date, int zip, int beds,
            int baths, int sq_ft, int price, double latitude, double longitude) {
        super(street, city, state, type, sale_date, zip, beds, baths, sq_ft, price, latitude, longitude);
    }

    public Unknown(Unknown u) {
        super(u);
    }

    @Override
    public String toString() {
        String s = super.toString();

        return s;
    }

    @Override
    public Unknown clone() {
        return new Unknown(getStreet(), getCity(), getState(), getType(), getSale_date(), getZip(), getBeds(),
                getBaths(), getSq_ft(), getPrice(), getLatitude(), getLongitude());
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof Unknown) {
                Unknown u = (Unknown) o;
                return super.equals(u);
            }
        }
        return false;
    }
}